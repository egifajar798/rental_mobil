<!-- Header-->
<header class="bg-dark py-5">
            <div class="container px-3 px-lg-3 my-3">
                <div class="text-center text-white">
                    <h1 class="display-5 fw-bolder">DOME CAR RENT</h1>
                    <p class="lead fw-normal text-white-50 mb-0">Car Rent Solution!</p>
                </div>

            </div>
        </header>
        <!-- Section-->

            <?php echo $this->session->flashdata('pesan') ?>

        <section class="container d-flex row mx-auto">
            <?php foreach($mobil2 as $mb) : ?>
            <div class="px-4 px-lg-5 mt-5 d-flex col-4">
                <div class="row justify-content-between m-auto">
                    <div class="col mb-5">
                        <div class="card h-100">
                            <!-- Product image-->
                            <a href="#"><img class="card-img-top" src="<?php echo base_url('assets/upload/'.$mb->gambar) ?>" style="width: 200px; height: 130px;"></a>
                            <!-- Product details-->
                            <div class="card-body p-4 d-flex">
                                <div class="text-center">
                                    <!-- Product name-->
                                    <h5 class="fw-bolder"><?php echo $mb->merk ?></h5>
                                    <!-- Product price-->
                                    <h5>No. Plat: <?php echo $mb->no_plat ?></h5>
                                    <h5>Rp.<?php echo  number_format($mb->harga,0,',','.') ?>/HARI</h5>
                                    <h5>Fasilitas</h5>
                                    <div class="card-footer">
                                        <li>
                                            <?php if($mb->ac == "1") {
                                        echo "<i class='text-warning'></i>";
                                    }else{
                                        echo "<i class='text-danger'></i>";
                                    }?>AC
                                        </li>

                                        <li>
                                            <?php if($mb->gps == "1") {
                                        echo "<i class='text-warning'></i>";
                                    }else{
                                        echo "<i class='text-danger'></i>";
                                    }?>GPS
                                        </li>

                                        <li>
                                            <?php if($mb->supir == "1") {
                                        echo "<i class='text-warning'></i>";
                                    }else{
                                        echo "<i class='text-danger'></i>";
                                    }?>Supir
                                        </li>

                                        <li>
                                            <?php if($mb->mp3_player == "1") {
                                        echo "<i class='text-warning'></i>";
                                    }else{
                                        echo "<i class='text-danger'></i>";
                                    }?>MP3 Player
                                        </li>
                                        
                                    </div>
                                </div>
                            </div>
                            <!-- Product actions-->
                            <div class="card-footer">

                                <?php 
                                if ($mb->status == "tersedia") {
                                    echo anchor('customer/rental/tambah_rental'.$mb->id_mobil, '<button class="btn btn-success">Tersedia</button>');
                                }else if ($mb->status == "dibooking") {
                                    echo anchor('costumer/rental/tambah_rental'.$mb->id_mobil, '<buttom class="btn btn-warning">Di booking</button>');
                                }
                                 ?>
                                  <a class="btn btn-dark" href="<?php echo base_url('customer/dashboard/detail_mobil/'.$mb->id_mobil) ?>">Detail </a>

                                </div>
                            </div>
                        </div>
                        
                    </div>
                       
                </div>
            </div>
                    
            <?php endforeach; ?>
        </section>
       
