<div class="container">
	<div class="card mx-auto" style="margin-top: 80px; margin-bottom: 80px; width: 80%;">
		<div class="card-header d-flex justify-content-between">
			<span>
				Status Mobil
			</span>
		</div>

		<div class="form-group">
			<form method="POST" action="<?php
											switch ($this->input->post('status')) {
												case 'dibooking':
													echo base_url('customer/cekbooking/dibooking');
													break;

												case 'tersedia':  
													echo base_url('customer/cekbooking/tersedia');
													break;
												
												default:
													# code...
													break;
											}
										?>">
				<select name="status" class="form-control">
					<option value="">Pilih Status</option>
					<option value="dibooking">DI booking</option>
					<option value="tersedia">Tersedia</option>
				</select>
				<?php echo form_error('status', '<div class="text=small text-danger">', '</div>') ?>
		</div>

		<button class="btn btn-secondary mt-3">Cek mobil</button>
		</form>
	</div>
</div>
</div>