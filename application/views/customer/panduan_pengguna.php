<center><div id="adobe-dc-view" style="height: 900px; width: 800px;"></div></center>

<script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
<script type="text/javascript">
  document.addEventListener("adobe_dc_view_sdk.ready", function(){
    var adobeDCView = new AdobeDC.View({clientId: "e6bda95ba62b43acb2c32e3da926fba3", divId: "adobe-dc-view"});
    adobeDCView.previewFile({
      content:{ location:
        { url: "<?php echo base_url(); ?>/assets/docs/User_Manual_Rental_Mobil.pdf"}},
      metaData:{fileName: "User Guide.pdf"}
    }, {});
  });
</script>
