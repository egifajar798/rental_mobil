<div class="main-content">
	<section class="section">
		<div class="section-header">
			<h1>Data Transaksi</h1>
		</div>

		<div class="navbar-form">
            <?php echo form_open('admin/transaksi/search') ?>
            <input type="text" name="keyword" class="form-control" placeholder="Cari transaksi">
            <button type="submit" class="btn btn-success mt-2 mb-2">Cari</button>
            <?php echo form_close() ?>	
        </div>

		<?php echo $this->session->flashdata('pesan') ?>
		<table class="table-responsive table table-bordered table-striped">
			<tr>
	 			<th>No</th>
				<th>Nama Customer</th>
				<th>Nama Mobil</th>
				<th>Tgl. Rental</th>
				<th>Tgl. Kembali</th>
				<th>Tgl. Dikembalikan</th>
				<th>Jumlah Hari Sewa</th>
				<th>Total Harga</th>
				<th>Total Denda</th>
				<th>Status Rental</th>
				<th>Cek Pembayaran</th>
				<th>Action</th>
			</tr>

			<?php $no=1;
			foreach($transaksi as $tr) : ?>

				<tr>
					<td><?php echo $no++ ?></td>
					<td><?php echo $tr->nama ?></td>
					<td><?php echo $tr->merk ?></td>
					<td><?php echo date('d/m/y', strtotime($tr->tanggal_rental)); ?></td>
					<td><?php echo date('d/m/y', strtotime($tr->tanggal_kembali)); ?></td>
					<td>
						<?php 
	
							if($tr->tanggal_pengembalian == "0000-00-00"){
								echo "-";
							}else{
								echo date('d/m/y', strtotime($tr->tanggal_pengembalian));
							}
						 ?>
					</td>

					<td>
						<?php 
						$x = strtotime($tr->tanggal_kembali);
						$y = strtotime($tr->tanggal_rental);
						$jmlHari = abs(($x - $y)/(60*60*24));
		 				?>
		 				
						<?php echo $jmlHari ?> Hari</td>
					<td>Rp. <?php echo number_format($tr->harga * $jmlHari,0,',','.') ?></td>
					<td>Rp.<?php echo number_format($tr->total_denda,0,',','.') ?></td>
					<td><?php echo $tr->status_rental ?></td>
					
					<td>
						<center>
							<?php 
							if(empty($tr->bukti_pembayaran)){ ?>
								<button class="btn btn-sm btn-danger"><i class="fas fa-times-circle"></i></button>
							<?php }else{ ?>
								<a class="btn btn-sm btn-primary" href="<?php echo base_url('admin/transaksi/pembayaran/'.$tr->id_rental) ?>"><i class="fas fa-check-circle"></i></a>
							<?php } ?>
						</center>
					</td>
					

					<td>
						<?php 

							if($tr->status == "1"){
								echo "-";
							}else{ ?>

								<div class="row">
									<a class="btn btn-sm btn-success mr-2" href="<?php echo base_url('admin/transaksi/transaksi_selesai/'.$tr->id_rental) ?>"><i class="fas fa-check"></i></a>
									<a onclick="return confirm('Apakah Anda Yakin?')" class="btn btn-sm btn-danger" href="<?php echo base_url('admin/transaksi/transaksi_batal/'.$tr->id_rental) ?>"><i class="fas fa-times"></i></a>
								</div>
							<?php } ?>

					</td>
				</tr>

			<?php endforeach; ?>
		</table>
	</section>
</div>
</div>