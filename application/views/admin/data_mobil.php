<div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Data Mobil</h1>
          </div>

          <a href="<?php echo base_url('admin/data_mobil/tambah_mobil') ?>" class="btn btn-primary mb-3">Tambah Data</a>

          <div class="navbar-form">
            <?php echo form_open('admin/data_mobil/search') ?>
            <input type="text" name="keyword" class="form-control" placeholder="Cari Mobil">
            <button type="submit" class="btn btn-success mt-2 mb-2">Cari</button>
            <?php echo form_close() ?>
          </div>
          <?php echo $this->session->flashdata('pesan') ?>

          <table width="80%" class="table table-striped table-responsive table-bordered ">
            <tr>
              <th>No</th>
              <th>Gambar</th>
              <th>Type</th>
              <th>Merk</th>
              <th>No. Plat</th>
              <th>Harga</th>
              <th>Denda/Hari</th>
              <th>Status</th>
              <th>Menu</th>
            </tr>

          <tbody>
            <?php 
                $no=1;
                foreach($mobil as $mb) : ?>
                    <tr>
                    <td><?php echo $no++ ?></td>
                    <td>
                      <img width="60px" src="<?php echo base_url().'assets/upload/'.$mb->gambar ?>">
                    </td>
                    <td><?php echo $mb->kode_type ?></td>
                    <td><?php echo $mb->merk ?></td>
                    <td><?php echo $mb->no_plat ?></td>
                    <td>Rp.<?php echo number_format($mb->harga,0,',','.') ?></td>
                    <td>Rp.<?php echo number_format($mb->denda,0,',','.') ?></td>
                    <td><?php 
                    if ($mb->status == "kosong") {
                      echo "<span class='badge badge-danger'>Persediaan Kosong</span>";
                  }else if ($mb->status == "dibooking") {
                      echo "<span class='badge badge-warning'>Di booking</span>";
                  }else {
                    echo "<span class='badge badge-primary'>Tersedia</span>";
                  }
                     ?></td>
                     <td>
                       <a href="<?php echo base_url('admin/data_mobil/detail/').$mb->id_mobil ?>" class="btn btn-sm btn-success"><i class="fas fa-eye"></i></a>
                       <a href="<?php echo base_url('admin/data_mobil/hapus/').$mb->id_mobil ?>" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></a>
                       <a href="<?php echo base_url('admin/data_mobil/update/').$mb->id_mobil ?>" class="btn btn-sm btn-primary"><i class="fas fa-edit"></i></a>
                     </td>
                     </tr>
            <?php endforeach; ?>
          </tbody>
          </table>
        </section>
      </div>