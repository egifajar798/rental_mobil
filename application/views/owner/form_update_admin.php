<div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Data Admin</h1>
          </div>
        </section>

        <?php foreach($admin as $adm) : ?>
        <form method="POST" action="<?php echo base_url('owner/data_admin/update_admin_aksi') ?>">

          <div class="form-group">
            <label>Nama</label>
            <input type="hidden" name="id_admin" value="<?php echo $adm->id_admin ?>">
            <input type="text" name="nama_admin" class="form-control" value="<?php echo $adm->nama_admin ?>">
            <?php echo form_error('nama_admin','<span class="text-small text-danger">','</span>') ?>
          </div>

          <div class="form-group">
            <label>Username</label>
            <input type="text" name="username" class="form-control" value="<?php echo $adm->username ?>">
            <?php echo form_error('username','<span class="text-small text-danger">','</span>') ?>
          </div>

          <div class="form-group">
            <label>Password</label>
            <input type="text" name="password" class="form-control" value="<?php echo $adm->password ?>">
            <?php echo form_error('password','<span class="text-small text-danger">','</span>') ?>
          </div>
          
          <button type="submit" class="btn btn-sm btn-primary">Submit</button>
          <button type="reset" class="btn btn-sm btn-danger">Reset</button>
        </form>

      <?php endforeach; ?>
</div>