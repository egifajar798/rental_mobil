<?php 

class Dashboard extends CI_Controller{

	public function index()
	{
		$admin = $this->db->query("SELECT * FROM admin");
		$data['admin']=$admin->num_rows();
		$this->load->view('templates_owner/header');
		$this->load->view('templates_owner/sidebar');
		$this->load->view('owner/dashboard', $data);
		$this->load->view('templates_owner/footer');
	}
}

 ?>