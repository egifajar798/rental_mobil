<?php 

class CekBooking extends CI_Controller{

	public function index()
	{
		$data['mobil'] = $this->model_mobil->get_data('mobil')->result();
		$data['mobil2'] = $this->model_mobil->get_status()->result();
		$this->load->view('templates_customer/header');
		$this->load->view('customer/cekbooking', $data);
		$this->load->view('templates_customer/footer');
	}
	public function dibooking()
	{
		$data['mobil'] = $this->model_mobil->get_data('mobil')->result();
		$data['mobil2'] = $this->model_mobil->get_status_dibooking()->result();
		$this->load->view('templates_customer/header');
		$this->load->view('customer/cekbooking', $data);
		$this->load->view('templates_customer/footer');
	}
	public function tersedia()
	{
		$data['mobil'] = $this->model_mobil->get_data('mobil')->result();
		$data['mobil2'] = $this->model_mobil->get_status_tersedia()->result();
		$this->load->view('templates_customer/header');
		$this->load->view('customer/cekbooking', $data);
		$this->load->view('templates_customer/footer');
	}
}

 ?>