<?php 

class Dashboard extends CI_Controller{

	public function index()
	{
		$admin = $this->db->query("SELECT * FROM admin");
		$customer = $this->db->query("SELECT * FROM customer");
		$mobil = $this->db->query("SELECT * FROM mobil");
		$data['admin']=$admin->num_rows();
		$data['customer']=$customer->num_rows();
		$data['mobil']=$mobil->num_rows();
		$this->load->view('templates_admin/header', $data);
		$this->load->view('templates_admin/sidebar');
		$this->load->view('admin/dashboard', $data);
		$this->load->view('templates_admin/footer');
	}
}

 ?>