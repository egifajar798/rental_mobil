<?php 

class Model_mobil extends CI_model{
	public function get_data($table){
		return $this->db->get($table);
	}

	public function get_where($where, $table){
		return $this->db->get_where($table, $where);
	}

	public function get_status()
	{
		return $this->db->select('*')->from('mobil')->where('status', 1)->get();
	}

	public function get_status_dibooking()
	{
		return $this->db->select('*')->from('mobil')->where('status', 'dibooking')->get();
	}

	public function get_status_tersedia()
	{
		return $this->db->select('*')->from('mobil')->where('status', 'tersedia')->get();
	}

	public function insert_data($data,$table){
		$this->db->insert($table,$data);
	}

	public function update_data($table, $data, $where){
		$this->db->update($table,$data,$where);
	}
	
	public function delete_data($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}

	public function ambil_id_mobil($id)
	{
		$hasil = $this->db->where('id_mobil', $id)->get('mobil');
		if($hasil->num_rows() > 0){
			return $hasil->result();
		}else{
			return false;
		}
	}

	public function cek_login()
	{
		$username = set_value('username');
		$password = set_value('password');

		$result = $this->db
						->where('username', $username)
						->where('password', md5($username))
						->limit(1)
						->get('customer');
		if($result->num_rows() > 0) {
			return $result->row();
		}else{
			return FALSE;
		}
	}

	public function update_password($where, $data, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	public function downloadPembayaran($id)
	{
		$query = $this->db->get_where('transaksi', array('id_rental' => $id));
		return $query->row_array();
	}

	public function get_keyword($keyword){
		$this->db->select('*');
		$this->db->from('customer');
		$this->db->like('id_customer', $keyword);
		$this->db->or_like('nama', $keyword);
		$this->db->or_like('username', $keyword);
		$this->db->or_like('alamat', $keyword);
		$this->db->or_like('gender', $keyword);
		$this->db->or_like('no_telepon', $keyword);
		$this->db->or_like('no_ktp', $keyword);
		$this->db->or_like('password', $keyword);
		return $this->db->get()->result();
	}

	public function get_keyword_mobil($keyword){
		$this->db->select('*');
		$this->db->from('mobil');
		$this->db->like('id_mobil',$keyword);
		$this->db->or_like('kode_type',$keyword);
		$this->db->or_like('merk',$keyword);
		$this->db->or_like('no_plat',$keyword);
		$this->db->or_like('warna',$keyword);
		$this->db->or_like('tahun',$keyword);
		$this->db->or_like('harga',$keyword);
		$this->db->or_like('denda',$keyword);
		$this->db->or_like('ac',$keyword);
		$this->db->or_like('supir',$keyword);
		$this->db->or_like('mp3_player',$keyword);
		$this->db->or_like('gps',$keyword);
		$this->db->or_like('gambar',$keyword);
		return $this->db->get()->result();
	}

	public function get_keyword_transaksi($keyword){
		$this->db->select('*');
		$this->db->from('transaksi');
		$this->db->like('id_rental', $keyword);
		$this->db->or_like('id_customer', $keyword);
		$this->db->or_like('id_mobil', $keyword);
		$this->db->or_like('tanggal_rental', $keyword);
		$this->db->or_like('tanggal_kembali', $keyword);
		$this->db->or_like('harga', $keyword);
		$this->db->or_like('denda', $keyword);
		$this->db->or_like('total_denda', $keyword);
		$this->db->or_like('tanggal_pengembalian', $keyword);
		$this->db->or_like('status_pengembalian', $keyword);
		$this->db->or_like('status_rental', $keyword);
		$this->db->or_like('bukti_pembayaran', $keyword);
		$this->db->or_like('status_pembayaran', $keyword);
		return $this->db->get()->result();
	}
}

 ?>