<?php 

class Data_admin extends CI_Controller{

	public function index()
	{
		$data['admin'] = $this->model_mobil->get_data('admin')->result();
		$this->load->view('templates_owner/header');
		$this->load->view('templates_owner/sidebar');
		$this->load->view('owner/data_admin',$data);
		$this->load->view('templates_owner/footer');
	}

	public function tambah_admin()
	{
		$this->load->view('templates_owner/header');
		$this->load->view('templates_owner/sidebar');
		$this->load->view('owner/form_tambah_admin');
		$this->load->view('templates_owner/footer');
	}

	public function tambah_admin_aksi()
	{
		$this->_rules();

		if($this->form_validation->run() == FALSE) {
			$this->tambah_admin();
		}else{
			$nama_admin 			= $this->input->post('nama_admin');
			$username 				= $this->input->post('username');
			$password				= md5($this->input->post('password'));

			$data = array (
				'nama_admin'			=> $nama_admin,
				'username'				=> $username,
				'password'				=> $password
			);

			$this->model_mobil->insert_data($data, 'admin');
			$this->session->set_flashdata('pesan', '<div class="alert alert-warning alert-dismissible fade show" role="alert">
				  Data Berhasil Ditambahkan.
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				  </button>
				</div>');
				redirect('owner/data_admin');
		}
	}

	public function update_admin($id)
	{
		$where = array('id_admin' => $id);
		$data['admin'] = $this->db->query("SELECT * FROM admin WHERE id_admin = '$id'")->result();
		$this->load->view('templates_owner/header');
		$this->load->view('templates_owner/sidebar');
		$this->load->view('owner/form_update_admin', $data);
		$this->load->view('templates_owner/footer');
	}

	public function update_admin_aksi()
	{
		$this->_rules();

		if($this->form_validation->run() == FALSE) {
			$this->update_admin();
		}else{
			$id 					=$this->input->post('id_admin');
			$nama_admin 			= $this->input->post('nama_admin');
			$username 				= $this->input->post('username');
			$password				= md5($this->input->post('password'));

			$data = array (
				'nama_admin'			=> $nama_admin,
				'username'				=> $username,
				'password'				=> $password
			);

			$where = array(
				'id_admin' => $id
			);

			$this->model_mobil->update_data('admin', $data, $where);
			$this->session->set_flashdata('pesan', '<div class="alert alert-warning alert-dismissible fade show" role="alert">
				  Data Berhasil Diupdate.
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				  </button>
				</div>');
				redirect('owner/data_admin');
		}

	}

	public function delete_admin($id)
	{
		$where = array('id_admin' => $id);
		$this->model_mobil->delete_data($where,'admin');
		$this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
				  Data Berhasil Dihapus.
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				  </button>
				</div>');
				redirect('owner/data_admin');
	}

	public function _rules()
	{
		$this->form_validation->set_rules('nama_admin','Nama Admin','required');
		$this->form_validation->set_rules('username','Username','required');
		$this->form_validation->set_rules('password','Password','required');
	}
}

 ?>