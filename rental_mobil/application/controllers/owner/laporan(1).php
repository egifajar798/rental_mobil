<?php 

class Laporan extends CI_Controller{
	public function index()
	{

		$dari = $this->input->post('dari');
		$sampai = $this->input->post('sampai');
	
		$this->_rules();

		if($this->form_validation->run() == FALSE) {
			$this->load->view('templates_owner/header');
			$this->load->view('templates_owner/sidebar');
			$this->load->view('owner/filterlaporan_owner');
			$this->load->view('templates_owner/footer');
		}else{
			$data['laporan'] = $this->db->query("SELECT * FROM transaksi tr, mobil mb, customer cs WHERE tr.id_mobil=mb.id_mobil AND tr.id_customer=cs.id_customer AND date(tanggal_rental) >= 
				'$dari' AND date(tanggal_rental) <= '$sampai'")->result();
			$this->load->view('templates_owner/header');
			$this->load->view('templates_owner/sidebar');
			$this->load->view('owner/tampilkanlaporan_owner', $data);
			$this->load->view('templates_owner/footer');
		}
	}

	public function printlaporan_owner()
	{
		$dari = $this->input->get('dari');
		$sampai = $this->input->get('sampai');
		$data['title'] = "Print Laporan Transaksi";
		$data['laporan'] = $this->db->query("SELECT * FROM transaksi tr, mobil mb, customer cs WHERE tr.id_mobil=mb.id_mobil AND tr.id_customer=cs.id_customer AND date(tanggal_rental) >= 
				'$dari' AND date(tanggal_rental) <= '$sampai'")->result();
			$this->load->view('templates_owner/header', $data);
			$this->load->view('owner/printlaporan_owner', $data);
	}

	public function _rules()
	{
		$this->form_validation->set_rules('dari', 'Dari Tanggal', 'required');
		$this->form_validation->set_rules('sampai', 'Sampai Tanggal', 'required');
	}
}

 ?>