<?php 

class Booking extends CI_Controller{

	public function index()
	{
		$this->load->view('templates_customer/header');
		$this->load->view('customer/booking');
		$this->load->view('templates_customer/footer');
	}

	public function cekbooking()
	{
		$data['mobil'] = $this->model_mobil->get_data('mobil')->result();
		$this->load->view('templates_customer/header');
		$this->load->view('customer/cekbooking', $data);
		$this->load->view('templates_customer/footer');
	}
}

 ?>