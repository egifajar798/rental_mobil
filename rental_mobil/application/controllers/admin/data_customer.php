<?php 

class Data_customer extends CI_Controller{

	public function index()
	{
		$this->load->library('pagination');

		$config['base_url']     = site_url('data_costumer/index');
		$config['total_rows']   = $this->db->count_all('customer');
		$config['per_page']     = 5;
		$config['uri_segment']  = 2;
		$choice                 = $config["total_rows"] / $config['per_page'];
		$config["num_links"]    = floor($choice);

		$config['first_link']	= 'First';
		$config['last_link']	= 'Last';
		$config['next_link']	= 'Next';
		$config['prev_link']	= 'Prev';
		$config['full_tag_open']= '<div class="paging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close'] = '</ul></nav></div>';
		$config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close'] = '</span></li>';
		$config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close'] = '</span></li>';
		$config['next_tag_open'] = '<li class="page-item active"><span class="page-link">';
		$config['next_tagl_close'] = '<span aria-hidden="true">&raquo</span></li>';
		$config['prev_tag_open'] = '<li class="page-item active"><span class="page-link">';
		$config['prev_tagl_close'] = '</span>Next</li>';
		$config['first_tag_open'] = '<li class="page-item active"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open'] = '<li class="page-item active"><span class="page-link">';
		$config['last_tagl_close'] = '</span></li>';
	

		$this->pagination->initialize($config);
		$data['page']	= ($this->uri->segment(2)) ? $this->uri->segment(2) : 0; 

		$data['customer'] = $this->model_mobil->get_data('customer', $config["per_page"], $data['page'])->result();
		$data['pagination'] = $this->pagination->create_links();
		$this->load->view('templates_admin/header');
		$this->load->view('templates_admin/sidebar');
		$this->load->view('admin/data_customer',$data);
		$this->load->view('templates_admin/footer');
		$data['customer'] = $this->model_mobil->get_data('customer')->result();
		$this->load->view('templates_admin/header');
		$this->load->view('templates_admin/sidebar');
		$this->load->view('admin/data_customer',$data);
		$this->load->view('templates_admin/footer');
	}
	public function tambah_customer()
	{
		$this->load->view('templates_admin/header');
		$this->load->view('templates_admin/sidebar');
		$this->load->view('admin/form_tambah_customer');
		$this->load->view('templates_admin/footer');
	}

	public function tambah_customer_aksi()
	{
		$this->_rules();

		if($this->form_validation->run() == FALSE) {
			$this->tambah_customer();
		}else{
			$nama 					= $this->input->post('nama');
			$username 				= $this->input->post('username');
			$alamat 				= $this->input->post('alamat');
			$gender 				= $this->input->post('gender');
			$no_telepon 			= $this->input->post('no_telepon');
			$no_ktp 				= $this->input->post('no_ktp');
			$password				= md5($this->input->post('password'));

			$data = array (
				'nama'					=> $nama,
				'username'				=> $username,
				'alamat'				=> $alamat,
				'gender'				=> $gender,
				'no_telepon'			=> $no_telepon,
				'no_ktp'				=> $no_ktp,
				'password'				=> $password
			);

			$this->model_mobil->insert_data($data, 'customer');
			$this->session->set_flashdata('pesan', '<div class="alert alert-warning alert-dismissible fade show" role="alert">
				  Data Berhasil Ditambahkan.
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				  </button>
				</div>');
				redirect('admin/data_customer');
		}
	}

	public function update_customer($id)
	{
		$where = array('id_customer' => $id);
		$data['customer'] = $this->db->query("SELECT * FROM customer WHERE id_customer = '$id'")->result();
		$this->load->view('templates_admin/header');
		$this->load->view('templates_admin/sidebar');
		$this->load->view('admin/form_update_customer', $data);
		$this->load->view('templates_admin/footer');
	}

	public function update_customer_aksi()
	{
		$this->_rules();

		if($this->form_validation->run() == FALSE) {
			$this->update_customer;
		}else{
			$id 					=$this->input->post('id_customer');
			$nama 					= $this->input->post('nama');
			$username 				= $this->input->post('username');
			$alamat 				= $this->input->post('alamat');
			$gender 				= $this->input->post('gender');
			$no_telepon 			= $this->input->post('no_telepon');
			$no_ktp 				= $this->input->post('no_ktp');
			$password				= md5($this->input->post('password'));

			$data = array (
				'nama'					=> $nama,
				'username'				=> $username,
				'alamat'				=> $alamat,
				'gender'				=> $gender,
				'no_telepon'			=> $no_telepon,
				'no_ktp'				=> $no_ktp,
				'password'				=> $password
			);

			$where = array(
				'id_customer' => $id
			);

			$this->model_mobil->update_data('customer', $data, $where);
			$this->session->set_flashdata('pesan', '<div class="alert alert-warning alert-dismissible fade show" role="alert">
				  Data Berhasil Diupdate.
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				  </button>
				</div>');
				redirect('admin/data_customer');
		}

	}

	public function delete_customer($id)
	{
		$where = array('id_customer' => $id);
		$this->model_mobil->delete_data($where,'customer');
		$this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
				  Data Berhasil Dihapus.
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				  </button>
				</div>');
				redirect('admin/data_customer');
	}

	public function _rules()
	{
		$this->form_validation->set_rules('nama','Nama','required');
		$this->form_validation->set_rules('username','Username','required');
		$this->form_validation->set_rules('alamat','Alamat','required');
		$this->form_validation->set_rules('gender','Gender','required');
		$this->form_validation->set_rules('no_telepon','No. Telepon','required');
		$this->form_validation->set_rules('no_ktp','No. KTP','required');
		$this->form_validation->set_rules('password','Password','required');
	}
}

 ?>