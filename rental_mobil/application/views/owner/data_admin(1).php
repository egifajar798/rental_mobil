<div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Data Admin</h1>
          </div>

          <a href="<?php echo base_url('owner/data_admin/tambah_admin') ?>" class="btn btn-primary mb-3">Tambah Admin</a>

          <?php echo $this->session->flashdata('pesan') ?>

          <table class="table table-striped table-bordered">
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Username</th>
              <th>Password</th>
              <th>Aksi</th>
            </tr>

            <?php 
            $no =1;
            foreach ($admin as $adm) : ?>
                <tr>
                  <td><?php echo $no++ ?></td>
                  <td><?php echo $adm->nama_admin ?></td>
                  <td><?php echo $adm->username ?></td>
                  <td><?php echo $adm->password ?></td>
                  <td>

                    <div class="row">

                    <a href="<?php echo base_url('owner/data_admin/delete_admin/'.$adm->id_admin) ?>" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></a>
                    <a href="<?php echo base_url('owner/data_admin/update_admin/'.$adm->id_admin) ?>" class="btn btn-sm btn-primary ml-3"><i class="fas fa-edit"></i></a>
                    </div>
                  </td>
                </tr>

            <?php endforeach; ?>
          </table>