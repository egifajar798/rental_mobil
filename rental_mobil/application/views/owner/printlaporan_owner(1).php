<h3 style="text-align: center;">Laporan Transaksi Rental Mobil</h3>
<table>
	<tr>

    <div class="login-brand">
              <img src="<?php echo base_url('assets/assets_stisla/') ?>/assets/img/logo.png" alt="logo" width="100" class="shadow-light rounded-circle">
            </div>
    
		<td>Dari Tanggal</td>
		<td>:</td>
		<td><?php echo date('d-M-Y', strtotime($_GET['dari'])) ?></td>
	</tr>
	<tr>
		<td>Sampai Tanggal</td>
		<td>:</td>
		<td><?php echo date('d-M-Y', strtotime($_GET['sampai'])) ?></td>
	</tr>
</table>

<table class="table table-bordered table-striped mt-3">
  <tr>
    <th>No</th>
    <th>Nama Customer</th>
    <th>Nama Mobil</th>
    <th>Tgl. Rental</th>
    <th>Tgl. Kembali</th>
    <th>Harga Sewa/Hari</th>
    <th>Denda/Hari</th>
    <th>Total Denda</th>
    <th>Tgl. Dikembalikan</th>
    <th>Status Pengembalian</th>
    <th>Status Rental</th>
  </tr>

  <?php $no=1;
  foreach($laporan as $tr) : ?>

    <tr>
      <td><?php echo $no++ ?></td>
      <td><?php echo $tr->nama ?></td>
      <td><?php echo $tr->merk ?></td>
      <td><?php echo date('d/m/y', strtotime($tr->tanggal_rental)); ?></td>
      <td><?php echo date('d/m/y', strtotime($tr->tanggal_kembali)); ?></td>
      <td>Rp.<?php echo number_format($tr->harga,0,',','.') ?></td>
      <td>Rp.<?php echo number_format($tr->denda,0,',','.') ?></td>
      <td>Rp.<?php echo number_format($tr->total_denda,0,',','.') ?></td>
      <td>
        <?php 

          if($tr->tanggal_pengembalian == "0000-00-00"){
            echo "-";
          }else{
            echo date('d/m/y', strtotime($tr->tanggal_pengembalian));
          }
         ?>
      </td>

      <td><?php echo $tr->status_pengembalian ?></td>

      <td><?php echo $tr->status_rental ?></td>


      
    </tr>

  <?php endforeach; ?>
</table>

 <h6><table width="100%">
    <tr>
      <td></td>
      <td width="200px">
        <p>Bandung, <?php echo date("d-M-Y") ?> <br> Owner</p>
        <br>
        <br>
        <br>
        <p>(..........................)</p>
      </td>
    </tr>
  </table>
  </h6>

<script type="text/javascript">
	window.print();
</script>