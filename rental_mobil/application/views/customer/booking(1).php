<div class="container">
	<div class="card mx-auto" style="margin-top: 80px; margin-bottom: 80px; width: 80%;">
		<div class="card-header">
		<!-- <div>
			Pilih Tanggal
		</div> -->
		<div>
			<a href="#" class="btn btn-primary">Cek Booking</a>
		</div>


		</div>

		<div class="card-body">
			<table class="table table-bordered table-striped">
			<form method="POST" action="<?php echo base_url('customer/booking') ?>">
	            <div class="form-group">
	              <input type="date" name="tanggal" class="form-control">
	              <?php echo form_error('tanggal','<span class="text-small text-danger">','</span>') ?>
	            </div>

	            <a class="btn btn-primary mt-3" href="customer/booking">Cek</a>

	        </form>
	    </table>
</div>
	</div>
</div>