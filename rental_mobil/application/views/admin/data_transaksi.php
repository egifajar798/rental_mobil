<div class="main-content">
	<section class="section">
		<div class="section-header">
			<h1>Data Transaksi</h1>
		</div>

		<?php echo $this->session->flashdata('pesan') ?>
		<table class="table-responsive table table-bordered table-striped">
			<tr>
				<th>No</th>
				<th>Nama Customer</th>
				<th>Nama Mobil</th>
				<th>Tgl. Rental</th>
				<th>Tgl. Kembali</th>
				<th>Status Rental</th>
				<th>Cek Pembayaran</th>
				<th>Action</th>
			</tr>

			<?php $no=1;
			foreach($transaksi as $tr) : ?>

				<tr>
					<td><?php echo $no++ ?></td>
					<td><?php echo $tr->nama ?></td>
					<td><?php echo $tr->merk ?></td>
					<td><?php echo date('d/m/y', strtotime($tr->tanggal_rental)); ?></td>
					<td><?php echo date('d/m/y', strtotime($tr->tanggal_kembali)); ?></td>
					<td><?php echo $tr->status_pengembalian ?></td>

					<td><?php echo $tr->status_rental ?></td>


					<td>
						<center>
							<?php 
							if(empty($tr->bukti_pembayaran)){ ?>
								<button class="btn btn-sm btn-danger"><i class="fas fa-times-circle"></i></button>
							<?php }else{ ?>
								<a class="btn btn-sm btn-primary" href="<?php echo base_url('admin/transaksi/pembayaran/'.$tr->id_rental) ?>"><i class="fas fa-check-circle"></i></a>
							<?php } ?>
						</center>
					</td>

					<td>
						<?php 

							if($tr->status == "1"){
								echo "-";
							}else{ ?>

								<div class="row">
									<a class="btn btn-sm btn-success mr-2" href="<?php echo base_url('admin/transaksi/transaksi_selesai/'.$tr->id_rental) ?>"><i class="fas fa-check"></i></a>
									<a onclick="return confirm('Apakah Anda Yakin?')" class="btn btn-sm btn-danger" href="<?php echo base_url('admin/transaksi/transaksi_batal/'.$tr->id_rental) ?>"><i class="fas fa-times"></i></a>
								</div>
							<?php } ?>

					</td>
				</tr>

			<?php endforeach; ?>
		</table>
	</section>
</div>
</div>